const { kafka } = require("./client");

async function init() {
    const admin = kafka.admin();
    admin.connect();
    console.log("Admin connected...");

    await admin.createTopics({
        topics: [
            {
                topic: "driver-updates",
                numPartitions: 2,
            },
        ],
    });
    console.log("Topic created - [driver-updates]");

    console.log("Disconnecting the Admin.");
    admin.disconnect();
}

init();
