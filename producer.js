const { kafka } = require("./client");
const readline = require("readline");

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
});

async function init() {
    const producer = kafka.producer();

    await producer.connect();
    console.log("Producer Connected...");

    rl.setPrompt("> ");
    rl.prompt();

    rl.on("line", async function (line) {
        const [driverName, location] = line.split(" ");

        await producer.send({
            topic: "driver-updates",
            messages: [
                {
                    partition: location.toLowerCase() === "north" ? 0 : 1,
                    key: "location-update",
                    value: JSON.stringify({
                        name: driverName,
                        loc: location,
                    }),
                },
            ],
        });
    }).on("close", async function () {
        console.log("Producer Sent successfully");

        await producer.disconnect();
        console.log("Producer disconnected...");
    });
}

init();
