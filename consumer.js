const { kafka } = require("./client");
const group = process.argv[2];

async function init() {
    const consumer = kafka.consumer({ groupId: group });

    await consumer.connect();
    console.log("Consumer Connected...");

    await consumer.subscribe({
        topics: ["driver-updates"],
        fromBeginning: true,
    });
    console.log("Consumer Subscribed...");

    await consumer.run({
        eachMessage: async ({
            topic,
            partition,
            message,
            heartbeat,
            pause,
        }) => {
            console.log(
                `${group}: [${topic}]: PART: ${partition}: }`,
                message.value.toString()
            );
        },
    });

    console.log("Consumer runs successfully");
}

init();
